<%-- 
    Document   : clientesListar
    Created on : Sep 18, 2018, 2:20:50 PM
    Author     : cassiano
--%>
<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${empty sessionScope.login}">
    <c:set var="errorMsg" value="Usuario deve se autenticar para acessar o sistema"></c:set>
    <jsp:forward page="/index.jsp">
        <jsp:param name="errorMsg" value="${errorMsg}"></jsp:param>
    </jsp:forward>
</c:if>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/templates/meta.html"%>
    <%@include file="/templates/header.html"%>
</head> 
    <%@include file="/templates/style.html"%>
<body> 
    <%@include file="/templates/navbar.html"%>
    <div class="w3-main" style="margin-left:340px;margin-right:40px">
    <h1 class="w3-jumbo"><b>Detalhes Atendimento</b></h1>
    <h1 class="w3-xxxlarge w3-text-black"><b>Atendimento</b></h1>
    <table id="t01" class=display>
    <thead>
    <tr>
      <th>ID</th>
      <th>Data</th>
      <th>Descrição</th>
      <th>TIPO</th>
      <th>Produto</th>
      <th>User</th>
      <th>Cliente</th>      
      <th>Resposta</th>  
      <th>Opções</th>  
    </tr>
  </thead>
    <tbody>
        <tr>
        <td>${atendimento.id}</td>
        <td>${atendimento.dt_hr}</td>
        <td>${atendimento.dsc}</td>
        <td>${atendimento.tipo_atendimento.nome}</td>        
        <td>${atendimento.produto.nome}</td>
        <td>${atendimento.usuario.nome_usuario}</td>
        <td>${atendimento.cliente.nome}</td>
        <td>${atendimento.resposta}</td>        
        <td>
        <a href="/web2ex8/AtendimentoServlet" style="color: red">Retornar</a>
        </td>        
        </tr>

  </tbody>  
</table>     
    </div>
    <%@include file="/templates/footer.jsp"%>
    <%@include file="/templates/js_scripts.html"%>
</body>
</html>
