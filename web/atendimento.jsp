<%-- 
    Document   : clientesForm
    Created on : Oct 23, 2018, 3:30:17 AM
    Author     : cassiano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${empty sessionScope.login}">
    <c:set var="errorMsg" value="Usuario deve se autenticar para acessar o sistema"></c:set>
    <jsp:forward page="/index.jsp">
        <jsp:param name="errorMsg" value="${errorMsg}"></jsp:param>
    </jsp:forward>
</c:if>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/templates/meta.html"%>
    <%@include file="/templates/header.html"%>
</head>
    <%@include file="/templates/style.html"%>
<body> 
    <%@include file="/templates/navbar.html"%>    
    <div class="w3-main" style="margin-left:340px;margin-right:40px">
    
    <c:set var="chamada" value="Novo Atendimento"></c:set>
    <c:set var="form_redirect" value="/web2ex8/AtendimentoServlet?action=NEW"></c:set>
    <c:set var="botao" value="Salvar"></c:set>   
    
    <h1 class="w3-jumbo"><b>${chamada}</b></h1>
    <h1 class="w3-xxxlarge w3-text-black"><b>Atendimento</b></h1>
    
    <form action="${form_redirect}" method="POST">        
        Id Usuario:<br/><input type='text' name="user_id" readonly="true" value=${sessionScope.login.id}><br/>        
        Nome Usuario:<br/><input type='text' name="user_name" readonly="true" value=${sessionScope.login.login}><br/>                
        Descrição:
        <br/><input type='text' name="dsc" value=${atendimento.dsc}><br/>
        Marque se seu problema foi resolvido:<br/><input type='checkbox' name="resposta" value="true"><br/>        
        Tipo Atendimento:
        <select id="tip" name="Tipo">
            <c:forEach var="tipo" items="${requestScope.tipos}">
                <option value="${tipo.id}">"${tipo.nome}"</option>                    
            </c:forEach>
        </select>                 
        Produto:
        <select id="produto" name="Produto">
            <c:forEach var="prod" items="${requestScope.produtos}">
                <option value="${prod.id}">"${prod.nome}"</option>                    
            </c:forEach>
        </select>        
        Cliente:
        <select id="cliente" name="Cliente">
            <c:forEach var="cli" items="${requestScope.clientes}">
                <option value="${cli.id}">"${cli.nome}"</option>                    
            </c:forEach>
        </select>        
        <input type='submit' value="${botao}"/>
    </form>
    <br> <br> <br>
    <form action="/web2ex8/AtendimentoServlet">
        <input type="submit" value="Retornar" />
    </form>    
    </div>
    <%@include file="/templates/footer.jsp"%>
    <%@include file="/templates/js_scripts.html"%>
    </body>
</html>
