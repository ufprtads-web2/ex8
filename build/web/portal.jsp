<%-- 
    Document   : portal
    Created on : Sep 17, 2018, 10:36:33 PM
    Author     : cassiano
--%>
<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${empty sessionScope.login}">
    <c:set var="errorMsg" value="Usuario deve se autenticar para acessar o sistema"></c:set>
    <jsp:forward page="/index.jsp">
        <jsp:param name="errorMsg" value="${errorMsg}"></jsp:param>
    </jsp:forward>
</c:if>
<!DOCTYPE html>
<jsp:useBean id="login" class="com.ufpr.tads.web2.beans.LoginBean"
scope="session"/>
<html>
    <head>
    <%@include file="templates/meta.html"%>
    <%@include file="templates/header.html"%>        
    </head>
    <%@ include file="templates/style.html" %>  
    <body>
    
    <%@include file="templates/navbar.html"%>                                
    <h1 align='right'>Bem vindo, <jsp:getProperty name="login" property="login"/></h1>    
    <%@include file="templates/footer.jsp"%>            
    <%@include file="/templates/js_scripts.html" %>        
    </body>
</html>
