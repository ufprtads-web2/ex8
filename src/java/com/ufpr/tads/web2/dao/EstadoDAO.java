/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Estado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cassiano
 */
public class EstadoDAO {
    
    private Connection con;

    public EstadoDAO() {
        this.con = ConnectionFactory.getConnection();
    }
    
    public List<Estado> selectEstados() throws SQLException{
        List<Estado> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM estado";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
              Estado estado = new Estado();  
              estado.setId(rs.getInt("id"));
              estado.setNome(rs.getString("nome"));
              estado.setUf(rs.getString("uf"));              
              resultados.add(estado);
        }        
        return resultados;        
    }
    
    public Estado selectEstado(String id) throws SQLException{
    
        String sql = "SELECT * FROM estado WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        Estado estado = new Estado();
        while (rs.next()){
            estado.setId(rs.getInt("id"));
            estado.setNome(rs.getString("nome"));
            estado.setUf(rs.getString("uf"));
            return estado;
        }    
        return null;
    }
    
    public void closeConnection() throws SQLException{
        con.close();        
    }
}
