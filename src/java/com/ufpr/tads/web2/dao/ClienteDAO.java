/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cassiano
 */
public class ClienteDAO {
    private Connection con;

    public ClienteDAO() {
        this.con = ConnectionFactory.getConnection();
    }
    
    public List<Cliente> selectClientes() throws SQLException {
        
        List<Cliente> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM cliente";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        
        while (rs.next()) {
              Cliente cliente = new Cliente();
              cliente.setId(rs.getInt("id"));
              cliente.setNome(rs.getString("nome"));
              cliente.setEmail(rs.getString("email"));
              cliente.setCep(rs.getString("cep"));
              cliente.setId_cidade(rs.getInt("id_cidade"));
              cliente.setCpf(rs.getString("cpf"));
              cliente.setNr(rs.getInt("nr"));
              cliente.setRua(rs.getString("rua"));
              cliente.setDataCliente(rs.getDate("data_cliente"));
              resultados.add(cliente);
        }        
        return resultados;
    }
    public Cliente selectCliente(String id) throws SQLException{
        String sql = "SELECT * FROM cliente WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        Cliente cliente = new Cliente();
        while (rs.next()){
            cliente.setId(rs.getInt("id"));
            cliente.setNome(rs.getString("nome"));
            cliente.setEmail(rs.getString("email"));
            cliente.setCep(rs.getString("cep"));
            cliente.setId_cidade(rs.getInt("id_cidade"));
            cliente.setCpf(rs.getString("cpf"));            
            cliente.setNr(rs.getInt("nr"));
            cliente.setRua(rs.getString("rua"));
            cliente.setDataCliente(rs.getDate("data_cliente"));            
            return cliente;
        }        
        return null;
    }
    public void removeCliente(String id) throws SQLException{
        String sql = "DELETE FROM cliente WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        boolean rs = st.execute();          
    }
    
    public void updateCliente(Cliente cli) throws SQLException{
        String sql = "UPDATE cliente SET cpf=(?),nome=(?),email=(?),rua=(?),nr=(?),cep=(?),id_cidade=(?) WHERE id=(?)";
        try (PreparedStatement st = con.prepareStatement(sql)) {
            st.setString(1, cli.getCpf());
            st.setString(2, cli.getNome());
            st.setString(3, cli.getEmail());
            st.setString(4, cli.getRua());
            st.setInt(5, cli.getNr());
            st.setString(6, cli.getCep());
            st.setInt(7, cli.getId_cidade());
            st.setInt(8, cli.getId());
            st.executeUpdate();
        }             
    }
    
    public void insertCliente(Cliente cli) throws SQLException{
        String sql = "INSERT into cliente (cpf, nome, email, rua, nr, cep, id_cidade) VALUES ((?),(?),(?),(?),(?),(?),(?))";
        try (PreparedStatement st = con.prepareStatement(sql)) {
            st.setString(1, cli.getCpf());
            st.setString(2, cli.getNome());
            st.setString(3, cli.getEmail());
            st.setString(4, cli.getRua());
            st.setInt(5, cli.getNr());
            st.setString(6, cli.getCep());
            st.setInt(7, cli.getId_cidade());
            st.executeUpdate();
            st.close();
        }         
    }    
    
    public void closeConnection() throws SQLException{
        con.close();        
    }
}
