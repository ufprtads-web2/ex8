/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author cassiano
 */
public class ProdutoDAO {
    
    private Connection con;

    public ProdutoDAO() {
        this.con = ConnectionFactory.getConnection();
    }    
    
    public List<Produto> selectProdutos() throws SQLException{
        List<Produto> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM produto";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            Produto prod = new Produto();              
            prod.setId(rs.getInt("id"));
            prod.setNome(rs.getString("nome"));    
            resultados.add(prod);
        }        
        return resultados;      
    }
    
    public Produto selectProduto(String id) throws SQLException{
        
        String sql = "SELECT * FROM produto WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        Produto prod = new Produto();
        while (rs.next()){
            prod.setId(rs.getInt("id"));
            prod.setNome(rs.getString("nome"));            
            return prod;
        }
        return null;
    }        
    
    public void closeConnection() throws SQLException{
        con.close();        
    }    
}
