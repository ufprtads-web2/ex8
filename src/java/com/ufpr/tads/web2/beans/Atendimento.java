/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.beans;

import java.io.Serializable;
import java.sql.Date;
import com.ufpr.tads.web2.facade.ProdutosFacade;
import com.ufpr.tads.web2.facade.ClientesFacade;
import com.ufpr.tads.web2.facade.TiposAtendimentoFacade;
import com.ufpr.tads.web2.facade.UsuarioFacade;
import java.sql.SQLException;

/**
 *
 * @author cassiano
 */
public class Atendimento implements Serializable{
    
    
    
//    id                  | int(11)      | NO   | PRI | NULL    | auto_increment |
//| dt_hr               | date         | YES  |     | NULL    |                |
//| dsc                 | varchar(255) | YES  |     | NULL    |                |
//| id_produto          | int(11)      | YES  | MUL | NULL    |                |
//| id_tipo_atendimento | int(11)      | YES  | MUL | NULL    |                |
//| id_usuario          | int(11)      | YES  | MUL | NULL    |                |
//| id_cliente          | int(11)      | YES  | MUL | NULL    |                |
//| res_atendimento     | char(1)      | YES  |     | NULL    |                |

    public int id;
    private String dsc;
    private Produto produto;
    private TipoAtendimento tipo_atendimento;
    private Usuario usuario;
    private Cliente cliente;
    private String resposta;   
    private Date dt_hr;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto prod) throws SQLException {
        
        String id_produto;
        id_produto = Integer.toString(prod.getId());
        this.produto = ProdutosFacade.getProduto(id_produto);                
    }

    public TipoAtendimento getTipo_atendimento() {
        return tipo_atendimento;
    }

    public void setTipo_atendimento(TipoAtendimento tipo_atendimento) throws SQLException {
        String id_tipo_atendimento;
        id_tipo_atendimento = Integer.toString(tipo_atendimento.getId());        
        this.tipo_atendimento = TiposAtendimentoFacade.getTipoAtendimento(id_tipo_atendimento);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) throws SQLException, ClassNotFoundException {
        String id_user;
        id_user = Integer.toString(usuario.getId_usuario());
        this.usuario = UsuarioFacade.getUsuario(id_user);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) throws SQLException {
        String id_cliente;
        id_cliente = Integer.toString(cliente.getId());
        this.cliente = ClientesFacade.getClient(id_cliente);
    }    

    public Date getDt_hr() {
        return dt_hr;
    }

    public void setDt_hr(Date dt_hr) {
        this.dt_hr = dt_hr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

//    public int getId_produto() {
//        return id_produto;
//    }
//
//    public void setId_produto(int id_produto) {
//        this.id_produto = id_produto;
//    }
//
//    public int getId_tipo_atendimento() {
//        return id_tipo_atendimento;
//    }
//
//    public void setId_tipo_atendimento(int id_tipo_atendimento) {
//        this.id_tipo_atendimento = id_tipo_atendimento;
//    }
//
//    public int getId_usuario() {
//        return id_usuario;
//    }
//
//    public void setId_usuario(int id_usuario) {
//        this.id_usuario = id_usuario;
//    }
//
//    public int getId_cliete() {
//        return id_cliete;
//    }
//
//    public void setId_cliete(int id_cliete) {
//        this.id_cliete = id_cliete;
//    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
    
    
}
