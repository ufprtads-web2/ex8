/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Usuario;
import com.ufpr.tads.web2.dao.UsuarioDAO;
import java.sql.SQLException;

/**
 *
 * @author cassiano
 */
public class UsuarioFacade {
    
    static UsuarioDAO dao = null;
    
    public static Usuario getUsuario(String id) throws SQLException, ClassNotFoundException{        
        Usuario user = null;
        if (id != null){
            dao = new UsuarioDAO();        
            try {                                    
                user = dao.selectUsuario(id);
            } catch (SQLException ex) {
                throw ex;
            }
            finally {
            dao.closeConnection();
            }            
        }
        return user;        
    }
    
}
