/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Estado;
import com.ufpr.tads.web2.dao.EstadoDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cassiano
 */
public class EstadosFacade {
    
    static EstadoDAO dao = null;
    static Estado estado = null;
    
    public static List listStates() throws SQLException{
        
        dao = new EstadoDAO();
        List<Estado> estados = new ArrayList<>();
        try {
            estados = dao.selectEstados();
            return estados;
        } catch (SQLException e) {
           Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, e); 
        }finally {
            dao.closeConnection();    
        }       
        return null;
    }
    
}
