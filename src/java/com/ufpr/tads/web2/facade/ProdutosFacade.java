/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Produto;
import com.ufpr.tads.web2.dao.ProdutoDAO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author cassiano
 */
public class ProdutosFacade {
    
    static ProdutoDAO dao = null;
    static Produto tipo = null;
    
    public static List listProdutos() throws SQLException{        
        dao = new ProdutoDAO();
        List<Produto> produtos;
        try {            
            produtos = dao.selectProdutos();
            return produtos;
        } catch (SQLException ex) {
           throw ex;
        }finally {
            dao.closeConnection();    
        }               
    }
    
    public static Produto getProduto(String id) throws SQLException{        
        Produto produto = null;
        if (id != null){
            dao = new ProdutoDAO();        
            try {                                    
                produto = dao.selectProduto(id);
            } catch (SQLException ex) {
                throw ex;
            }
            finally {
            dao.closeConnection();
            }            
        }
        return produto;        
    }
}