/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Atendimento;
import com.ufpr.tads.web2.dao.AtendimentoDAO;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cassiano
 */
public class AtendimentosFacade {
    
    static AtendimentoDAO dao = null;
    static Atendimento atendimento = null;
    public static List listAtendimentos() throws SQLException, ClassNotFoundException{
        dao = new AtendimentoDAO();
        atendimento = new Atendimento();
        List<Atendimento> resultado = null;                    
        try {
            resultado = dao.selectAtendimentos();            
        } catch (SQLException ex) {
            throw ex;                
        }
        finally {
            dao.closeConnection();                       
        }
        return resultado;
    }
    public static Atendimento getAtendimento(String id) throws SQLException, ClassNotFoundException{        
        atendimento = null;
        if (id != null){
            dao = new AtendimentoDAO();        
            try {                                    
                atendimento = dao.selectAtendimento(id);
            } catch (SQLException ex) {
                throw ex;
            }
            finally {
            dao.closeConnection();
            }            
        }
        return atendimento;        
    }
    public static void deleteAtendimento(String id) throws SQLException{        
        if (id != null){                       
            dao = new AtendimentoDAO();
            try {
                dao.removeAtendimento(id);            
            } catch (SQLException ex) {
                throw ex;
            }
            finally {
                dao.closeConnection();
            }        
        }
    }
//    
//    public static void updateAtendimento(Atendimento c) throws SQLException{          
//        if (c != null){
//            try {
//                dao = new AtendimentoDAO();
//                dao.updateAtendimento(c);            
//            } catch (SQLException ex) {
//                throw ex;
//            } finally {
//                dao.closeConnection();
//            }            
//        }
//    }
//    
    public static void addAtendimento(Atendimento atendimento) throws SQLException{
//        if (cli != null){
        try {            
            dao = new AtendimentoDAO();
            dao.insertAtendimento(atendimento);
        } catch (SQLException ex) {
              ex.printStackTrace();
              throw ex;
        }finally {
            dao.closeConnection();
        }                
    }
//
//    static void addAtendimento(Atendimento atendimento, int atendimento_id) throws SQLException {        
//        dao = new AtendimentoDAO();
//        dao.insertAtendimento(atendimento, atendimento_id);
//        dao.closeConnection();                
//    }
}
